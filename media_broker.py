import qi
import requests
import json

from requests.adapters import HTTPAdapter

class MediaBroker:
    def play(self, name, media_file):
        session = requests.Session()
        session.mount('http://' + name, HTTPAdapter(max_retries=0))

        try:
                resp = session.get('http://' + name, timeout=2)
        except requests.exceptions.Timeout:
                raise Exception('I cannot communicate with the requested machine')

        if resp.status_code != 200:
                return False

        resp = session.post('http://' + name,
                             data=json.dumps({"media_file": media_file}),
                             headers={'Content-type': 'application/json'})

        if resp.status_code == 404:
            raise Exception('the requested media file does not exist')

        elif resp.status_code == 400:
            raise Exception('The media device is already in use')

        return resp.status_code == 200

    def stop(self):
        session = requests.Session()
        session.mount('http://' + name, HTTPAdapter(max_retries=0))

        try:
                resp = session.get('http://' + name, timeout=2)
        except requests.exceptions.Timeout:
                raise Exception('I cannot communicate with the requested machine')

        if resp.status_code != 200:
                return False

        resp = session.post('http://' + name,
                             data=json.dumps({"stop": True}),
                             headers={'Content-type': 'application/json'})

        return resp.status_code == 200

def main():
    app = qi.Application()
    app.start()

    media_broker = MediaBroker()
    app.session.registerService('MediaBroker', media_broker)
    app.run()

if __name__ == '__main__':
    main()
